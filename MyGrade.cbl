       IDENTIFICATION DIVISION.
       PROGRAM-ID. MYGRADE.
      *>  AUTHOR. SITTHICHAI.


      *>  read file mygrade.txt to write file avg.txt
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT MYGRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
      *>  read mygrade.txt file
       FD  MYGRADE-FILE.
       01  MYGRADE-DETAIL.
           88 END-OF-MYGRADE-FILE VALUE HIGH-VALUE.
           05 SUBJECT-ID           PIC X(6).
           05 SUBJECT-NAME         PIC X(50).
           05 SUBJECT-CREDIT-FILE  PIC 9(1).
           05 SUBJECT-GRADE        PIC X(2).
      *>  write avg.txt file
       FD  AVG-FILE.
       01  ABC-DETAIL.
           88 END-OF-ABC-DETAIL VALUE HIGH-VALUE.
           05 RESULT-GRADE      PIC 9(1)V999.
       WORKING-STORAGE SECTION.
      *>  grade avg all
       
       01  GRADE-CREDIT-TOTAL       PIC 9(1)V9   VALUE ZERO .
       01  GRADE-TOTAL              PIC 9(1)V999 VALUE ZERO .
       01  GRADE-AVG                PIC 9(1)V9   VALUE ZERO .
       01  GRADE-AVG-COUNT          PIC 9(2)     VALUE ZERO .
       01  GRADE-AVG-SUM            PIC 9(3)V999 VALUE ZERO .
      *CREDIT ALL
       01  GRADE-AVG-NUM            PIC 9(3)V999 VALUE ZERO .

      *>  grade avg cs 31
       01  GRADE-CS-CREDIT-TOTAL    PIC X.
       01  GRADE-CS-AVG             PIC 9(1)V999 VALUE ZERO .
       01  GRADE-AVG-CS-COUNT       PIC 9(1)V9   VALUE ZERO .
       01  GRADE-AVG-CS-SUM         PIC 9(3)     VALUE ZERO .

      *>  grade avg sci 3
       01  GRADE-SCI-CREDIT-TOTAL   PIC X.
       01  GRADE-AVG-SCI-COUNT      PIC 9(1)V9   VALUE ZERO .
       01  GRADE-SCI-AVG            PIC X.
       01  GRADE-AVG-CREDITS        PIC 9(9)     VALUE ZERO .

       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT MYGRADE-FILE
           OPEN OUTPUT AVG-FILE
           PERFORM UNTIL END-OF-MYGRADE-FILE
              READ MYGRADE-FILE
                 AT END SET END-OF-MYGRADE-FILE TO TRUE
              END-READ

              IF NOT END-OF-MYGRADE-FILE THEN
                 COMPUTE GRADE-AVG-COUNT = GRADE-AVG-COUNT + 1
                 PERFORM 001-PROCESS THRU 001-EXIT
                 MOVE SUBJECT-CREDIT-FILE TO GRADE-CREDIT-TOTAL
                 DISPLAY "AVG AND CREDIT TOTAL = "GRADE-AVG "/"
                 GRADE-CREDIT-TOTAL
                 DISPLAY "-"
                   COMPUTE GRADE-AVG-NUM = GRADE-AVG-NUM 
                   + GRADE-CREDIT-TOTAL
                   DISPLAY "SUBJECT-CREDIT-FILE = " SUBJECT-CREDIT-FILE
                   COMPUTE GRADE-AVG-SUM = GRADE-AVG-SUM +
                   (GRADE-AVG * GRADE-CREDIT-TOTAL)                  

                   DISPLAY "BEFORE GRADE-AVG-SUM = "  GRADE-AVG-SUM

              END-IF
           END-PERFORM
           COMPUTE GRADE-TOTAL = GRADE-AVG-SUM / GRADE-AVG-NUM
           DISPLAY "GRADE-AVG-NUM = " GRADE-AVG-NUM
           DISPLAY "GRADE-AVG-COUNT = " GRADE-AVG-COUNT
           DISPLAY "GRADE-AVG-SUM = " GRADE-AVG-SUM
           DISPLAY "GRADE-TOTAL = " GRADE-TOTAL
           CLOSE MYGRADE-FILE
           CLOSE AVG-FILE
           GOBACK
       .
      *>  fun process code
       001-PROCESS.
           DISPLAY SUBJECT-ID " " SUBJECT-NAME " " SUBJECT-CREDIT-FILE
           " " SUBJECT-GRADE

      *>      DISPLAY "AVG"
           IF SUBJECT-GRADE = "A"
              MOVE 4 TO GRADE-AVG
           ELSE IF SUBJECT-GRADE = "B+"
              MOVE 3.5 TO GRADE-AVG
           ELSE IF SUBJECT-GRADE = "B"
              MOVE 3   TO GRADE-AVG
           ELSE IF SUBJECT-GRADE  = "C+"
              MOVE 2.5 TO GRADE-AVG
           ELSE IF SUBJECT-GRADE  = "C"
              MOVE 2 TO GRADE-AVG
           ELSE IF SUBJECT-GRADE = "D+"
              MOVE 1.5 TO GRADE-AVG
           ELSE IF SUBJECT-GRADE = "D"
              MOVE 1 TO GRADE-AVG
           END-IF
       .
       001-EXIT.
       EXIT
       .

       002-SUM
       .
       002-EXIT.
       EXIT
       .
